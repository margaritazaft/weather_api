require 'rest-client'

class HealthController < ApplicationController
  def health
    render json: { message: "OK" }
  end
end
