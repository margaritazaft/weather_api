require 'rest-client'
require 'json'

class WeatherController < ApplicationController
  def current
    responce = RestClient.get('http://dataservice.accuweather.com/currentconditions/v1/294021?apikey=Fjrtukj96UwOeEyUg1Oqpl7yd09Afa5J')
    data = (JSON.parse responce.body).first["Temperature"]["Metric"]
    weather = Weather.create!(timedate: DateTime.now, temperature: data['Value'])
    render json: data
  end

  def historical
    responce = RestClient.get('http://dataservice.accuweather.com/currentconditions/v1/294021/historical/24?apikey=Fjrtukj96UwOeEyUg1Oqpl7yd09Afa5J')
    data = (JSON.parse responce.body)
    to_front_data = []
    data.each do |d|
      hash = {}
      datetime = Time.at(d["EpochTime"]).to_datetime
      hash["datetime"] = datetime
      temp = d["Temperature"]["Metric"]
      hash["temperature"] = temp
      to_front_data << hash
    end
    render json: to_front_data
  end

  def historical_max
    responce = RestClient.get('http://dataservice.accuweather.com/currentconditions/v1/294021/historical/24?apikey=Fjrtukj96UwOeEyUg1Oqpl7yd09Afa5J')
      data = (JSON.parse responce.body)
      to_front_data = []
      data.each do |d|
        temp = d["Temperature"]["Metric"]["Value"]
        to_front_data << temp
      end
     max_day_tamp = to_front_data.max
    render json: max_day_tamp
  end

  def historical_min
    responce = RestClient.get('http://dataservice.accuweather.com/currentconditions/v1/294021/historical/24?apikey=Fjrtukj96UwOeEyUg1Oqpl7yd09Afa5J')
      data = (JSON.parse responce.body)
      to_front_data = []
      data.each do |d|
        temp = d["Temperature"]["Metric"]["Value"]
        to_front_data << temp
      end
    min_day_tamp = to_front_data.min
    render json: min_day_tamp
  end

  def historical_avg
    responce = RestClient.get('http://dataservice.accuweather.com/currentconditions/v1/294021/historical/24?apikey=Fjrtukj96UwOeEyUg1Oqpl7yd09Afa5J')
      data = (JSON.parse responce.body)
      to_front_data = []
      data.each do |d|
        temp = d["Temperature"]["Metric"]["Value"]
        to_front_data << temp
      end
    avg_day_tamp = (to_front_data.sum / to_front_data.size).round(2)
    render json: avg_day_tamp
  end

  def by_time
    time = params[:time] #in routes .../:time
    temperature = Weather.where(datetime: Time.at(time).to_datetime)
    render json: temperature
  end
end
