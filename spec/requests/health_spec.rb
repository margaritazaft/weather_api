require 'swagger_helper'

describe 'Health API' do
  path 'health' do
    get 'Get information about the server' do
      tags 'health'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end
end