require 'swagger_helper'

describe 'Weather API' do
  path 'weather/current' do
    get 'Get information about current weather' do
      tags 'weather'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end

  path 'weather/historical' do
    get 'Get information about the weather in the past 24 hours' do
      tags 'weather'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end

  path 'weather/historical/max' do
    get 'Get information about the maximum temperature in the past 24 hours' do
      tags 'weather'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end
  
  path 'weather/historical/min' do
    get 'Get information about the mininmum temperature in the past 24 hours' do
      tags 'weather'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end

  path 'weather/historical/avg' do
    get 'Get information about the average temperature in the past 24 hours' do
      tags 'weather'
      consumes 'application/json'
  
      response '200', 'OK' do
        run_test!
      end
    end
  end

  path 'weather/by_time/{time}' do
    get 'Get information about the temperature in the specific time' do
      tags 'weather'
      consumes 'application/json'
      parameter name: 'time', in: :path, schema: {
        type: :string
      }
      response '200', 'OK' do
        run_test!
      end
      response '404', 'not found' do
        run_test!
      end
    end
  end
end