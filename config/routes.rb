Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get 'weather/current', to: 'weather#current'
  get 'weather/historical', to: 'weather#historical'
  get 'weather/historical/max', to: 'weather#historical_max'
  get 'weather/historical/min', to: 'weather#historical_min'
  get 'weather/historical/avg', to: 'weather#historical_avg'
  get 'weather/by_time/:time', to: 'weather#by_time'
  get 'health', to: 'health#health'


end
