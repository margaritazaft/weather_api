class CreateWeathers < ActiveRecord::Migration[7.0]
  def change
    create_table :weathers do |t|
      t.integer :temperature, null: false
      t.integer :timedate, null: false

      t.timestamps
    end
  end
end
